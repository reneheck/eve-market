package de.hecksolutions.ccp.eve.database.api.wallet;

import com.beimin.eveapi.character.wallet.transactions.WalletTransactionsParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.wallet.transactions.ApiWalletTransaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

import java.util.ArrayList;

/**
 * Created by cerias on 20.06.14.
 */
public class Transaction {

    ApiKey apiKey;

    ArrayList<ApiWalletTransaction> response = new ArrayList<ApiWalletTransaction>();
    ObjectMapper mapper = new ObjectMapper();
    ElasticSearchService es = ElasticSearchService.getInstance();

    public Transaction(ApiKey apiKey){
        this.apiKey = apiKey;
    }


    public void update(){
        try {
            if(apiKey.getApi().getAPIKeyInfo().isCorporationKey()) {
                for (int i = 1000; i < 1007; i++)
                    response.addAll(com.beimin.eveapi.corporation.wallet.transactions.WalletTransactionsParser.getInstance().getResponse(apiKey.getApi().getAuth(),i).getAll());
            }else {
                response.addAll(WalletTransactionsParser.getInstance().getTransactionsResponse(this.apiKey.getApi().getAuth()).getAll());
            }
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(ApiWalletTransaction entry:response) {
                entry.setCharacterID(apiKey.getApi().getAuth().getCharacterID());
                bulkRequest.add(es.getClient().prepareIndex(apiKey.getIndex(apiKey.getApi().getAuth().getCharacterID()).get("index"),"transaction",entry.getTransactionID()+"_"+entry.getTransactionDateTime())
                                .setSource(mapper.writeValueAsString(entry))

                );

            }
            if(bulkRequest.numberOfActions()>0)
            es.bulkInsert(bulkRequest);


        }  catch (ApiException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
