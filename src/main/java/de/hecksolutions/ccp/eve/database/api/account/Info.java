package de.hecksolutions.ccp.eve.database.api.account;

import com.beimin.eveapi.account.apikeyinfo.ApiKeyInfoResponse;
import com.beimin.eveapi.exception.ApiException;
import de.hecksolutions.ccp.eve.ApiKey;

/**
 * Created by cerias on 20.06.14.
 */
public class Info {
    ApiKey apiKey;

    public Info(ApiKey apiKey){
        this.apiKey=apiKey;
    }


    public ApiKeyInfoResponse requestInfo(){
        ApiKeyInfoResponse apiKeyInfoResponse = null;
        try {
            apiKeyInfoResponse=apiKey.getApi().getAPIKeyInfo();
        }  catch (ApiException e) {
            e.printStackTrace();
        }
        return apiKeyInfoResponse;
    }
}
