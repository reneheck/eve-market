package de.hecksolutions.ccp.eve.database.api.wallet;

import com.beimin.eveapi.character.marketorders.MarketOrdersParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.marketorders.ApiMarketOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.springframework.beans.factory.support.ManagedList;

import java.util.ArrayList;

/**
 * Created by cerias on 22.06.14.
 */
public class MarketOrder {

    ApiKey apiKey;
    ArrayList<ApiMarketOrder> marketOrders = new ManagedList<ApiMarketOrder>();
    ObjectMapper mapper = new ObjectMapper();
    ElasticSearchService es = ElasticSearchService.getInstance();

    public MarketOrder(ApiKey apiKey){
        this.apiKey = apiKey;
    }


    public void update(){
        try {
            if(apiKey.getApi().getAPIKeyInfo().isCorporationKey()) {
                for(int i = 1000;i<1007;i++)

                    marketOrders.addAll(com.beimin.eveapi.corporation.marketorders.MarketOrdersParser.getInstance().getResponse(apiKey.getApi().getAuth()).getAll());
            }else {
                marketOrders.addAll(MarketOrdersParser.getInstance().getResponse(apiKey.getApi().getAuth()).getAll());
            }
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(ApiMarketOrder entry:marketOrders) {


                bulkRequest.add(es.getClient().prepareIndex(apiKey.getIndex(apiKey.getApi().getAuth().getCharacterID()).get("index"),"marketOrder",entry.getOrderID()+"")
                                .setSource(mapper.writeValueAsString(entry))
                );

            }
            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);


        }  catch (ApiException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
