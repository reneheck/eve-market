package de.hecksolutions.ccp.eve.database.api.wallet;


import com.beimin.eveapi.character.accountbalance.AccountBalanceParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.accountbalance.AccountBalanceResponse;
import com.beimin.eveapi.shared.accountbalance.EveAccountBalance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by cerias on 20.06.14.
 */
public class Wallet {

    ApiKey apiKey;

    AccountBalanceResponse accountBalanceResponse = null;
    ArrayList<EveAccountBalance> accountBalances = new ArrayList<EveAccountBalance>();
    ObjectMapper mapper = new ObjectMapper();
    ElasticSearchService es = ElasticSearchService.getInstance();

    public Wallet(ApiKey apiKey){
        this.apiKey=apiKey;
        try {
            if(apiKey.getApi().getAPIKeyInfo().isCorporationKey()){
                accountBalanceResponse = com.beimin.eveapi.corporation.accountbalance.AccountBalanceParser.getInstance().getResponse(apiKey.getApi().getAuth());
            }else{
                accountBalanceResponse = AccountBalanceParser.getInstance().getResponse(apiKey.getApi().getAuth());
            }


            accountBalances.addAll(accountBalanceResponse.getAll());

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    public HashMap<String,ArrayList<EveAccountBalance>>  getAccountBalances(){
        HashMap<String,ArrayList<EveAccountBalance>> map = new HashMap<String,ArrayList<EveAccountBalance>>();
        map.put(apiKey.getOwnerName().get("name"),accountBalances);
        return map;
    }
    public void save()  {
        try {


        ArrayList wallet = new ArrayList();

            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(EveAccountBalance accountBalance:accountBalances){
                bulkRequest.add(es.getClient().prepareIndex(apiKey.getIndex(apiKey.getApi().getAuth().getCharacterID()).get("index"), "wallet", apiKey.getOwnerName().get("characterID") + "_" + accountBalance.getAccountKey())
                                .setSource(mapper.writeValueAsString(accountBalance))
                );
            }

            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

}
