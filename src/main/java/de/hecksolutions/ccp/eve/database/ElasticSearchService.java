package de.hecksolutions.ccp.eve.database;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.spi.resource.Singleton;
import de.hecksolutions.ccp.eve.ApiKey;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import javax.annotation.PreDestroy;
import java.util.ArrayList;

/**
 * Created by cerias on 07.06.14.
 */

@Singleton
public class ElasticSearchService  {
    private static ElasticSearchService INSTANCE = new ElasticSearchService();

    Client client;
    Settings settings;
    private String INDEX  = "market";
    ObjectMapper mapper = new ObjectMapper();

    private ElasticSearchService(){
        System.out.println("ES Starting...");
        settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", "elasticsearch").build();
        client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
        System.out.println("...ES Started!");

    }

    public static ElasticSearchService getInstance() {
        return INSTANCE;
    }

    @PreDestroy
    private void shutdown() {
        System.out.println("ES Stopping...");
    }

    public void init() {
        // do some initialization work
        //System.out.println("ES Starting...");
    }

    public ArrayList<ApiKey> getApis(){
        ArrayList<ApiKey> apiKeyArrayList = new ArrayList<ApiKey>();

        SearchRequestBuilder searchRequestBuilder = client.prepareSearch(INDEX)
                .setTypes("apikey")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.queryString("*"));

        SearchResponse response = searchRequestBuilder.execute()
                .actionGet();

        for(SearchHit hit:response.getHits().getHits()){
            if(!hit.id().isEmpty()&&hit.getSource().containsKey("key")) {

                ApiKey key = new ApiKey();
                key.setId(Integer.parseInt(hit.getId()));
                String keyValue= hit.getSource().get("key").toString();
                key.setKey(keyValue);
                apiKeyArrayList.add(key);
            }

        }
        return apiKeyArrayList;
    }



    public void insert(String json,String type,String id){


        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            System.out.print(response);

    }

    public void update(String json,String type,String id){

        DeleteResponse deleteResponse = client.prepareDelete(INDEX,type,id)
                .execute()
                .actionGet();

        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            System.out.print(response);

    }

    public String indizies(){
        String indizies = "leer";

        //client.admin().indices().create(CreateIndexRequest)
        return indizies;
    }

    public Client getClient(){
        return client;
    }

    public String getINDEX(){
        return INDEX;
    }

    public void bulkInsert(BulkRequestBuilder bulkRequest) {

        BulkResponse bulkResponse = bulkRequest.execute().actionGet();

        if (bulkResponse.hasFailures()) {
            System.out.println(bulkResponse.buildFailureMessage());
        }

    }

    /**
     *
     * @param bulkRequest
     * @param json
     * @param type
     * @param id
     * @return
     */
    private BulkRequestBuilder bulkEntry(BulkRequestBuilder bulkRequest, String json,String type,String id) {


                bulkRequest.add(client.prepareIndex(INDEX, type, id)
                                .setSource(json)
                );

        return bulkRequest;

    }


}
