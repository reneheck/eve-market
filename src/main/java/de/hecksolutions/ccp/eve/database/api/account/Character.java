package de.hecksolutions.ccp.eve.database.api.account;

import com.beimin.eveapi.account.characters.EveCharacter;
import com.beimin.eveapi.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

import java.util.ArrayList;

/**
 * Created by cerias on 20.06.14.
 */
public class Character {

    ApiKey apiKey;
    ObjectMapper mapper = new ObjectMapper();
    ElasticSearchService es = ElasticSearchService.getInstance();
    ArrayList<EveCharacter> eveCharacterSet = new ArrayList<EveCharacter>();

    public Character(ApiKey apiKey){
        this.apiKey=apiKey;
        try {
            eveCharacterSet.addAll(apiKey.getApi().getCharacters());
        }  catch (ApiException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<EveCharacter> requestCharacter(){


        return eveCharacterSet;
    }


    public void update(){



        if(eveCharacterSet.size()>0){
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(EveCharacter entry:eveCharacterSet) {
                String index= "error";
                String id = "error";
                String type = "error";
                if(apiKey.getApiKeyInfo().isCorporationKey()){
                    index = "corp_"+entry.getCorporationID();
                    id=entry.getCorporationName();
                    type = "corporation";
                }else{
                    index= "char_"+entry.getCharacterID();
                    id=entry.getName();
                    type = "character";
                }
                try {
                    bulkRequest.add(es.getClient().prepareIndex(index,type,id)
                                    .setSource(mapper.writeValueAsString(entry))
                    );
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

            }
            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);

        }

    }
}
