package de.hecksolutions.ccp.eve.database.api.account;

import com.beimin.eveapi.account.accountstatus.EveAccountStatus;
import com.beimin.eveapi.exception.ApiException;
import de.hecksolutions.ccp.eve.ApiKey;

/**
 * Created by cerias on 20.06.14.
 */
public class Status {

    ApiKey apiKey;

    public Status(ApiKey apiKey){
        this.apiKey=apiKey;
    }


    public EveAccountStatus requestStatus(){
        EveAccountStatus accountStatus = null;
        try {
            accountStatus=apiKey.getApi().getAccountStatus();
        }  catch (ApiException e) {
            e.printStackTrace();
        }
        return accountStatus;
    }
}
