package de.hecksolutions.ccp.eve.database.api.wallet;

import com.beimin.eveapi.character.wallet.journal.WalletJournalParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.wallet.journal.ApiJournalEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

import java.util.ArrayList;

/**
 * Created by cerias on 20.06.14.
 */
public class Jornal {
    ApiKey apiKey;

    ArrayList<ApiJournalEntry> walletJournalResponse = new ArrayList<ApiJournalEntry>();
    ObjectMapper mapper = new ObjectMapper();
    ElasticSearchService es = ElasticSearchService.getInstance();

    public Jornal(ApiKey apiKey){
        this.apiKey = apiKey;
    }


    public void update(){
        try {
            if(apiKey.getApi().getAPIKeyInfo().isCorporationKey()) {
                for(int i = 1000;i<1007;i++)
                    walletJournalResponse.addAll(com.beimin.eveapi.corporation.wallet.journal.WalletJournalParser.getInstance().getResponse(apiKey.getApi().getAuth(),i).getAll());
            }else {
                walletJournalResponse.addAll(WalletJournalParser.getInstance().getWalletJournalResponse(apiKey.getApi().getAuth()).getAll());
            }
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(ApiJournalEntry entry:walletJournalResponse) {


                bulkRequest.add(es.getClient().prepareIndex(apiKey.getIndex(apiKey.getApi().getAuth().getCharacterID()).get("index"),"jornal",entry.getRefID()+"_"+entry.getDate())
                        .setSource(mapper.writeValueAsString(entry))
                );

            }
            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);


        }  catch (ApiException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
