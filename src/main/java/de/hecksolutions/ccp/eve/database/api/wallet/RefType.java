package de.hecksolutions.ccp.eve.database.api.wallet;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.eve.reftypes.ApiRefType;
import com.beimin.eveapi.eve.reftypes.RefTypesParser;
import com.beimin.eveapi.eve.reftypes.RefTypesResponse;
import com.beimin.eveapi.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.bulk.BulkRequestBuilder;

/**
 * Created by cerias on 29.06.14.
 */
public class RefType {
    ElasticSearchService es = ElasticSearchService.getInstance();
    ObjectMapper mapper = new ObjectMapper();
    public RefType(){
        EveApi api = new EveApi();
        try {
            RefTypesResponse response = new RefTypesParser().getResponse();
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            for(ApiRefType type:response.getAll()){

                bulkRequest.add(es.getClient().prepareIndex("eve","refType",type.getRefTypeID()+"")
                                .setSource(mapper.writeValueAsString(type))
                );

            }
            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }


}
