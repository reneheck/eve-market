package de.hecksolutions.ccp.eve.database.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 * Created by cerias on 29.06.14.
 */
public class Station {
    ElasticSearchService es = ElasticSearchService.getInstance();
    ObjectMapper mapper = new ObjectMapper();
    public Station(Statement statement){

        try {
            IndicesExistsRequestBuilder stationExists = es.getClient().admin().indices().prepareExists("eve","station");

            if(stationExists.execute().actionGet().isExists()) {

                CountRequestBuilder countRequestBuilder = es.getClient().prepareCount("eve", "station");
                CountResponse response = countRequestBuilder.execute().actionGet();
                System.out.println(response);
            }
            BulkRequestBuilder bulkRequest =  es.getClient().prepareBulk();
            ResultSet rs = statement.executeQuery( "select stationID,stationName from staStations;" );
            while ( rs.next() ) {
                HashMap<String,String> map = new HashMap<String, String>();
                map.put("name",rs.getString("stationName"));
                bulkRequest.add(es.getClient().prepareIndex("eve","station",rs.getInt("stationID")+"")
                        .setSource(mapper.writeValueAsString(map)));
            }

            if(bulkRequest.numberOfActions()>0)
                es.bulkInsert(bulkRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
