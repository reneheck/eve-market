package de.hecksolutions.ccp.eve;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.account.accountstatus.EveAccountStatus;
import com.beimin.eveapi.account.apikeyinfo.ApiKeyInfoResponse;
import com.beimin.eveapi.account.characters.EveCharacter;
import com.beimin.eveapi.core.ApiAuthorization;
import com.beimin.eveapi.shared.accountbalance.EveAccountBalance;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import de.hecksolutions.ccp.eve.database.api.account.Info;
import de.hecksolutions.ccp.eve.database.api.account.Status;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by cerias on 02.06.14.
 */
public class ApiKey {

    int id;
    String key;
    @JsonIgnore
    EveApi api;

    EveAccountStatus status;
    ApiKeyInfoResponse apiKeyInfo;



    HashMap<String,ArrayList<EveAccountBalance>> wallet = new HashMap<String,ArrayList<EveAccountBalance>>();

    public ApiKey(){}
    public ApiKey(String t) {
        this.id = 1369502;
        this.key = "aSzwStR4wikwqwEQFwlLqNeS3vKVJvvZDBoz81c0Gzhdp2tA8h9AZlMpAGpbhVww";
        api = new EveApi();
        api.setAuth(this.getAuth());
//        Character c = new Character(this);
//        characters.addAll(c.requestCharacter());
//        Status s = new Status(this);
//        status = s.requestStatus();
//        Info i = new Info(this);
//        apiKeyInfo = i.requestInfo();
    }




    public ApiKey(int id, String key) {
        this.id = id;
        this.key = key;
        api = new EveApi();
        api.setAuth(this.getAuth());
        Status s = new Status(this);
        status = s.requestStatus();
        Info i = new Info(this);
        apiKeyInfo = i.requestInfo();

        this.save();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public HashMap<String,ArrayList<EveAccountBalance>> getWallet() {
        return wallet;
    }

    public void setWallet(HashMap<String,ArrayList<EveAccountBalance>> wallet) {
        this.wallet = wallet;
    }

    public void addWallet(HashMap<String,ArrayList<EveAccountBalance>> wallet) {
        this.wallet.putAll(wallet);
    }

    @JsonIgnore

    public ApiAuthorization getAuth(){
        ApiAuthorization auth = new ApiAuthorization(this.id, this.key);

        return auth;
    }

    public EveApi getApi(){
        return api;
    }

    public EveAccountStatus getStatus() {
        return status;
    }

    public void setStatus(EveAccountStatus status) {
        this.status = status;
    }

    public ApiKeyInfoResponse getApiKeyInfo() {
        return apiKeyInfo;
    }

    public void setApiKeyInfo(ApiKeyInfoResponse apiKeyInfo) {
        this.apiKeyInfo = apiKeyInfo;
    }

    @JsonIgnore
    public void save(){
        ElasticSearchService es = ElasticSearchService.getInstance();
        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
           json = mapper.writeValueAsString(this);
            es.insert(json, "apikey", this.id + "");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @JsonIgnore
    public HashMap<String,String> getIndex(long characterID){
        HashMap<String,String> map = new HashMap<String, String>();
        String index= "error";
        String id = "error";


        if(getApiKeyInfo().isCorporationKey()){
            for(EveCharacter character:apiKeyInfo.getEveCharacters())
                if(character.getCharacterID()==characterID) {
                    index = "corp_" + character.getCorporationID();
                    id = character.getCorporationName();
                }
        }else{
            for(EveCharacter character:apiKeyInfo.getEveCharacters())
                if(character.getCharacterID()==characterID) {
                    index = "char_" + character.getCharacterID();
                    id = character.getName();
                }
        }

        map.put("index",index);
        map.put("id",id);

        return map;
    }

    @JsonIgnore
    public void update(){
        ElasticSearchService es = ElasticSearchService.getInstance();
        ObjectMapper mapper = new ObjectMapper();
        String json;
        try {
            json = mapper.writeValueAsString(this);
            es.update(json, "apikey", this.id + "");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @JsonIgnore
    public void updateData(){
        api = new EveApi();
        api.setAuth(this.getAuth());
        if(status==null) {
            Status s = new Status(this);
            status = s.requestStatus();
        }
        if(apiKeyInfo==null) {
            Info i = new Info(this);
            apiKeyInfo = i.requestInfo();
        }
    }

    @JsonIgnore
    public HashMap<String,String> getOwnerName(){
        HashMap<String,String> name = new HashMap<String, String>();
        EveCharacter character = null;
        for(EveCharacter eveCharacter :apiKeyInfo.getEveCharacters())
            if(eveCharacter.getCharacterID()==api.getAuth().getCharacterID()||eveCharacter.getCorporationID()==api.getAuth().getCharacterID())
                character = eveCharacter;

        if(apiKeyInfo.isCorporationKey()&&character!=null){
            name.put("name",character.getCorporationName());
            name.put("characterID",character.getCorporationID()+"");
        }else{
            name.put("name",character.getName());
            name.put("characterID",character.getCharacterID()+"");
        }



        return name;
    }
}
