package de.hecksolutions.ccp.eve;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**:::::
 * Creasted by cerias on 07.06.14.CodsecCodec::::::::::
 */
public class Start {

    public static void main(String[] args) throws IOException {



        HttpServer server = new HttpServer();
        NetworkListener listener = new NetworkListener("grizzly2", "localhost", 3388);
        server.addListener(listener);

        server.getServerConfiguration().addHttpHandler(
                new CLStaticHttpHandler(Start.class.getClassLoader()), "/");



        // Initialize and add Spring-aware Jersey resource
        WebappContext ctx = new WebappContext("ctx", "/api");
        final ServletRegistration reg = ctx.addServlet("spring", new SpringServlet());
        reg.addMapping("/*");
        ctx.addContextInitParameter("contextConfigLocation", "classpath:spring-context.xml");
        ctx.addListener("org.springframework.web.context.ContextLoaderListener");
        ctx.addListener("org.springframework.web.context.request.RequestContextListener");
        ctx.deploy(server);



        // Add the StaticHttpHandler to serve static resources from the webapp.static1 folder


//        // Add the CLStaticHttpHandler to serve static resources located at
//        // the static2 folder from the jar file jersey1-grizzly2-spring-1.0-SNAPSHOT.jar



        try {
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:sqlite-latest.sqlite");

            //Station station = new Station(c.createStatement());

            ElasticSearchService es = ElasticSearchService.getInstance();
//           ApiKey apiKey = new ApiKey(1369502,"aSzwStR4wikwqwEQFwlLqNeS3vKVJvvZDBoz81c0Gzhdp2tA8h9AZlMpAGpbhVww");
//            apiKey.save();



        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        try {

            server.start();
           

//            RefType refType = new RefType();


            System.out.println();
            System.out.println("Press enter to stop the server...");
            System.in.read();
        } finally {

            System.out.println("Shutting down...");
            server.shutdownNow();
        }


    }
}
