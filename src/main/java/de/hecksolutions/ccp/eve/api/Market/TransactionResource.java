package de.hecksolutions.ccp.eve.api.Market;

import com.beimin.eveapi.character.wallet.transactions.WalletTransactionsParser;
import com.beimin.eveapi.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by cerias on 02.06.14.
 */
@Component
@Path("/transaction")
public class TransactionResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt(){
        String json = "";
        ApiKey key = new ApiKey("t");
        ObjectMapper mapper = new ObjectMapper();

        try {
            json = mapper.writeValueAsString(WalletTransactionsParser.getInstance().getTransactionsResponse(key.getAuth()));
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return json;
    }
}
