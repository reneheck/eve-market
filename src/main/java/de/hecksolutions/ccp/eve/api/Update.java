package de.hecksolutions.ccp.eve.api;

import com.beimin.eveapi.account.characters.EveCharacter;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import de.hecksolutions.ccp.eve.database.api.account.Character;
import de.hecksolutions.ccp.eve.database.api.wallet.Jornal;
import de.hecksolutions.ccp.eve.database.api.wallet.MarketOrder;
import de.hecksolutions.ccp.eve.database.api.wallet.Transaction;
import de.hecksolutions.ccp.eve.database.api.wallet.Wallet;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by cerias on 20.06.14.
 */
@Component
@Path("update")
public class Update {

    ElasticSearchService es = ElasticSearchService.getInstance();
    ObjectMapper mapper = new ObjectMapper();
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String updateAll(){
        String returnValue ="oh noes!";

        for(ApiKey apiKey:es.getApis()){
            //TODO: activate after testing
                apiKey.updateData();

            if(apiKey.getApiKeyInfo().isAccountKey()){
                for(EveCharacter eveCharacter: apiKey.getApiKeyInfo().getEveCharacters()){
                    apiKey.getApi().selectCharacter(eveCharacter);
                    character(apiKey);

                }
            }

            if(apiKey.getApiKeyInfo().isCharacterKey()){

                for(EveCharacter eveCharacter: apiKey.getApiKeyInfo().getEveCharacters()){
                    apiKey.getApi().selectCharacter(eveCharacter);
                    character(apiKey);
                }

            }

            if(apiKey.getApiKeyInfo().isCorporationKey()){
                for(EveCharacter eveCharacter: apiKey.getApiKeyInfo().getEveCharacters()){
                    apiKey.getApi().selectCharacter(eveCharacter);
                    character(apiKey);
                }
            }

        }

        return returnValue;
    }


    private void character(ApiKey apiKey){
        Wallet wallet = new Wallet(apiKey);
        apiKey.addWallet(wallet.getAccountBalances());
        wallet.save();
        Jornal jornal = new Jornal(apiKey);
        jornal.update();
        Transaction transaction = new Transaction(apiKey);
        transaction.update();
        de.hecksolutions.ccp.eve.database.api.account.Character character = new Character(apiKey);
        character.update();
        MarketOrder marketOrder = new MarketOrder(apiKey);
        marketOrder.update();

        apiKey.update();
    }
}
