package de.hecksolutions.ccp.eve.api.Account;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.account.characters.EveCharacter;
import com.beimin.eveapi.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * Created by cerias on 02.06.14.
 */
@Component
@Path("account/characters")
public class AccountCharacters {

    ElasticSearchService es = ElasticSearchService.getInstance();
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        String json = "";

        EveApi api = new EveApi();
        ApiKey key = new ApiKey("t");
        api.setAuth(key.getAuth());
        ObjectMapper mapper = new ObjectMapper();

        try {
            ArrayList<EveCharacter> characterArrayList = new ArrayList<EveCharacter>();
            characterArrayList.addAll(api.getCharacters());
            json = mapper.writeValueAsString(characterArrayList);
            for(EveCharacter character:characterArrayList){

                es.insert(mapper.writeValueAsString(character),"character",character.getCharacterID()+"");
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return json;
    }
}
