package de.hecksolutions.ccp.eve.api.Market;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.character.wallet.journal.WalletJournalParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.wallet.journal.WalletJournalResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by cerias on 02.06.14.
 */
@Component
@Path("jornal")
public class JornalResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        ApiKey key = new ApiKey("t");

        WalletJournalParser walletJournalParser = WalletJournalParser.getInstance();
        WalletJournalResponse walletJournalResponse = null;

        ObjectMapper mapper = new ObjectMapper();
        String json = "no!";
        EveApi api = new EveApi();

        System.out.println("jornal call");
        try {
            walletJournalResponse = walletJournalParser.getWalletJournalResponse(key.getAuth());
            json = mapper.writeValueAsString(walletJournalResponse.getAll());
        }  catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return json;
    }
}
