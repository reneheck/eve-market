package de.hecksolutions.ccp.eve.api.Account;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.character.accountbalance.AccountBalanceParser;
import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.shared.accountbalance.AccountBalanceResponse;
import com.beimin.eveapi.shared.accountbalance.EveAccountBalance;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;

/**
 * Created by cerias on 07.06.14.
 */
@Component
@Path("wallet")
public class WalletResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        ApiKey key = new ApiKey("t");

        AccountBalanceParser accountBalanceParser = AccountBalanceParser.getInstance();
        AccountBalanceResponse accountBalanceResponse = null;

        ObjectMapper mapper = new ObjectMapper();
        String json = "no!";
        EveApi api = new EveApi();


        try {
            accountBalanceResponse = accountBalanceParser.getResponse(key.getAuth());
            HashMap<String,Object> map = new HashMap<String, Object>();
            for(EveAccountBalance accountBalance:accountBalanceResponse.getAll()){
                map.put("accountID",accountBalance.getAccountID());

                map.put("accountKey",accountBalance.getAccountKey());
                map.put("balance",accountBalance.getBalance());
                map.put("readTime",accountBalanceResponse.getCurrentTime());
                mapper.writeValueAsString(map);

            }
            json = mapper.writeValueAsString(accountBalanceResponse.getAll());
        }  catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return json;
    }
}
