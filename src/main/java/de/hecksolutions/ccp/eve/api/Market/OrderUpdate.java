package de.hecksolutions.ccp.eve.api.Market;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.database.ElasticSearchService;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by cerias on 29.06.14.
 */
@Component
@Path("/orderUpdate")
public class OrderUpdate {

    ElasticSearchService es = ElasticSearchService.getInstance();
    ObjectMapper mapper = new ObjectMapper();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt(){
        String returnValue= "";

        SearchRequestBuilder searchRequestBuilder = es.getClient().prepareSearch("_all")
                .setTypes("marketOrder")
                .setQuery(QueryBuilders.queryString("orderState:0"));

        SearchResponse response = searchRequestBuilder.execute().actionGet();

        System.out.print(response.getHits().getTotalHits());
        ArrayList<Map> map = new ArrayList<Map>();
        for(SearchHit hit :response.getHits().getHits()){
            map.add(hit.getSource());

        }
        try {
            returnValue = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //

        return returnValue;
    }
}
