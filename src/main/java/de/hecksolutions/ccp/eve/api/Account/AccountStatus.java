package de.hecksolutions.ccp.eve.api.Account;

import com.beimin.eveapi.EveApi;
import com.beimin.eveapi.exception.ApiException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by cerias on 02.06.14.
 */
@Component
@Path("account/status")
public class AccountStatus {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        String json = "";

        EveApi api = new EveApi();
        ApiKey key = new ApiKey("t");
        api.setAuth(key.getAuth());
        ObjectMapper mapper = new ObjectMapper();

        try {
            json = mapper.writeValueAsString(api.getAccountStatus());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        }

        return json;
    }
}
