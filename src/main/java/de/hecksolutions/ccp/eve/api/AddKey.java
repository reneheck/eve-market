package de.hecksolutions.ccp.eve.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hecksolutions.ccp.eve.ApiKey;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by cerias on 15.07.14.
 */
@Component
@Path("add")
public class AddKey {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String add(String json){

        ObjectMapper mapper = new ObjectMapper();
        TypeReference<HashMap<String,String>> typeRef
                = new TypeReference<HashMap<String,String>>() {};
        try {
            HashMap<String,String> o = mapper.readValue(json, typeRef);
            ApiKey key = new ApiKey(Integer.parseInt(o.get("id")),o.get("hash"));
            key.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ok";
    }
}
